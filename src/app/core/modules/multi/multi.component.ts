import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multi',
  templateUrl: './multi.component.html',
  styleUrls: ['./multi.component.css']
})
export class MultiComponent implements OnInit {

  constructor() { }
  @Output() changeLang: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {
  }
  public changeLanguage(language) {
    this.changeLang.emit(language);
  }

}
